import { createRouter, createWebHistory } from 'vue-router'
import App from 'F:/Programación/c3/NTS/Tech_shop/nts_fe/src/router/App.vue'

import Intelcorei7 from '../components/Intelcorei7.vue'
import Intelcorei5 from '../components/Intelcorei5.vue'
import Logitech from '../components/Logitech.vue'
import Memoria8GCrucial from '../components/Memoria8GCrucial.vue'
import Monitordell from '../components/Monitordell.vue'
import Nvidia2060s from '../components/Nvidia2060s.vue'
import Parlantesgenius from '../components/Parlantesgenius.vue'
import Redragon from '../components/Redragon.vue'
import LogIn from '../components/LogIn.vue'
import SignUp from '../components/SignUp.vue'
import ProcesandoCompra from '../components/ProcesandoCompra.vue'
import Home from '../components/Home.vue'
import TarjetasGraficas from '../components/TarjetasGraficas.vue'
import Procesadores from '../components/Procesadores.vue'
import MemoriasRAM from '../components/MemoriasRAM.vue'
import Monitores from '../components/Monitores.vue'
import Perifericos from '../components/Perifericos.vue'
import Accesorios from '../components/Accesorios.vue'

const routes = [
    {
 path: '/',
 name: 'root',
 component: App
},
{
    path: '/login',
    name: 'LogIn',
    component: LogIn
   },
   {
    path: '/signup',
    name: 'SignUp',
    component: SignUp
   },
{
 path: '/intelcorei7',
 name: 'Intelcorei7',
 component: Intelcorei7
},
{
    path: '/intelcorei5',
    name: 'Intelcorei5',
    component: Intelcorei5
   },
   {
    path: '/logitech',
    name: 'Logitech',
    component: Logitech
   },
   {
    path: '/memoria8gcrucial',
    name: 'Memoria8GCrucial',
    component: Memoria8GCrucial
   },
   {
    path: '/monitordell',
    name: 'Monitordell',
    component: Monitordell
   },
   {
    path: '/nvidia2060s',
    name: 'Nvidia2060s',
    component: Nvidia2060s
   },
   {
    path: '/parlantesgenius',
    name: 'Parlantesgenius',
    component: Parlantesgenius
   },
   {
    path: '/redragon',
    name: 'Redragon',
    component: Redragon
   },
   {
    path: '/procesandocompra',
    name: 'ProcesandoCompra',
    component: ProcesandoCompra
   },
   {
    path: '/home',
    name: 'Home',
    component: Home
   },
   {
    path: '/tarjetasgraficas',
    name: 'TarjetasGraficas',
    component: TarjetasGraficas
   },
   {
    path: '/procesadores',
    name: 'Procesadores',
    component: Procesadores
   },
   {
   path: '/memoriasRAM',
   name: 'MemoriasRAM',
   component: MemoriasRAM
  },
  {
    path: '/monitores',
    name: 'Monitores',
    component: Monitores
   },
   {
    path: '/perifericos',
    name: 'Perifericos',
    component: Perifericos
   },
   {
    path: '/accesorios',
    name: 'Accesorios',
    component: Accesorios
   },
]
const router = createRouter({
    
 history: createWebHistory(),
 routes
})
export default router